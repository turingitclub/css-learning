# 任务二：零基础 HTML 及 CSS 编码（二）

**面向人群：** 零基础或初学者

**难度：** 简单

## 重要说明

我们尽力保证课程内容的质量以及学习难度的合理性，但即使如此，真正决定课程效果的，还是你的每一次思考和实践。

课程多数题目的解决方案都不是唯一的，这和我们在实际工作中的情况也是一致的。因此，我们的要求不仅仅是实现设计稿的效果，更是要多去思考不同的解决方案，评估不同方案的优劣，然后使用在该场景下最优雅的方式去实现。那些最终没有被我们采纳的方案，同样也可以帮助我们学到很多知识。所以，我们列出的参考资料未必是实现需求所必须的。有的时候，实现题目的要求很简单，甚至参考资料里就有，但是背后的思考和亲手去实践却是任务最关键的一部分。在学习这些资料时，要多思考，多提问，多质疑。相信通过和小伙伴们的交流，能让你的学习事半功倍。

## 任务目的

* 针对设计稿样式进行合理的 HTML 架构，包括以下但不限于：
* 掌握常用 HTML 标签的含义、用法
* 能够基于设计稿来合理规划 HTML 文档结构
* 理解语义化，合理地使用 HTML 标签来构建页面
* 掌握基本的 CSS 编码，包括以下但不限于：
* 了解 CSS 的定义、概念、发展简史
* 掌握 CSS 选择器的含义和用法
* 实践并掌握 CSS 的颜色、字体、背景、边框、盒模型、简单布局等样式的定义方式

## 任务描述

* 基于第一个任务“零基础 HTML 编码”的代码，参考示例图

![](./task_1_2_1.jpg) 
* 在步骤一的代码基础上增加 CSS 样式代码的编写
* 头部和底部的黑色区域始终是 100%宽
* 页面右侧部分为固定宽度，左侧保持与浏览器窗口变化同步自适应变化
* 左侧的各个模块里面的内容宽度跟随左侧整体宽度同步自适应变化
* 10 张图片需要永远都完整展现，所以会随着宽度变窄，从两行变成三行甚至更多，也有可能随着宽度变宽，变成一行

## 任务注意事项

* 只需要完成 HTML，CSS 代码编写，不需要写 JavaScript
* 示例图仅为参考，不需要完全实现一致，其中的图片、文案均可自行设定
* 尽可能多地尝试不同的、更多的样式设定来实践各种 CSS 属性
* HTML 及 CSS 代码结构清晰、规范
* 可以结合自己所学添加新样式

## 任务协作建议

* 团队集中讨论，明确题目要求，保证队伍各自对题目要求认知一致
* 可以各自完成任务实践，也可以按照以下分模块然后各自分工完成
* 头部、底部及整体左右布局架构
* 右侧表单
* 前面两篇文章部分
* 图片部分
* 表格部分
* 交叉互相 Review 其他人的代码，建议每个人至少看一个同组队友的代码
* 相互讨论，最后合成一份组内最佳代码进行提交

## 在线学习参考资料

* [CSS 上](https://www.yuque.com/docs/share/36a18785-2279-48a4-8548-a752a4ab4416?#)
  
* [CSS 中](https://www.yuque.com/docs/share/416ae31e-dc5f-422e-9025-a6f8c35a78ae?#)

* [CSS 下](https://www.yuque.com/docs/share/419284bd-1036-405c-86bc-b21f2ff49c91?#)

* [MDN HTML 入门](https://developer.mozilla.org/zh-CN/docs/Web/Guide/HTML/Introduction)
* [MDN CSS 入门教程](https://developer.mozilla.org/zh-CN/docs/Web/Guide/CSS/Getting_started)


## 学生笔记

> 各位学员可将笔记写在此位置

比如 :

### css: 常用的元素居中方法

日常工作中常常会遇到元素居中的需求，通常块级元素的水平居中只需左右margin设为auto即可。
而行间元素的居中则是由父级设置行高(等于父级高度)和 text-align(center)实现。
但是如果要求是块级元素水平垂直居中呢？本文总结了一些常用的元素水平垂直居中方法。

### absolute
##### 1. 定位实现居中(需计算偏移值)
```html
<div class="absolute_p1">
  <div class="absolute_c1"></div>
</div>
```
```css
.absolute_p1 {
  position: relative;

  width: 200px; height: 200px;
}
.absolute_p1 .absolute_c1 {
  position: absolute; /* fixed 同理 */
  left: 50%;          top: 50%;

  width: 100px;       height: 100px;
  margin-left: -50px; margin-top: -50px;  /* 需根据宽高计算偏移量 */
}
```
- 原理: 通过定位使元素左上角居中，再通过偏移值margin调整使元素中心居中
- 缺点：高度宽度需事先知道，得通过其来计算负margin(好烦)

##### 1. 定位实现居中(不需计算偏移值)
```html
<div class="absolute_p2">
  <div class="absolute_c2"></div>
</div>
```
```css
.absolute_p2 {
  position: relative;

  width: 200px; height: 200px;
}
.absolute_p2 .absolute_c2 {
  position: absolute; /* fixed 同理 */
  left: 0; top: 0; bottom: 0; right: 0; /* 定位为 0 */

  width: 100px; height: 100px;
  margin: auto; /* 不用计算偏移量 */
}
```
- 原理: 原理我也不知道啊！估计定位都给0了，元素自己也不知道该去哪儿，只好待在原地不知所措...

##### 3. 定位实现居中(不需计算偏移值)
```html
<div class="absolute_p3">
  <div class="absolute_c3"></div>
</div>
```
```css
.absolute_p3 {
  position: relative;

  width: 200px; height: 200px;
}
.absolute_p3 .absolute_c3 {
  position: absolute; /* fixed 同理 */
  left: 50%; top: 50%;

  width: 100px; height: 100px;
  transform: translate(-50%, -50%);
}
```
- 原理: 通过定位使元素左上角居中，再通过 translate 位移元素使之中心居中，由于 translate 支持百分比，所以也就不用自己算偏移量了
- 缺点: ie 就别想用了

***

## table
```html
<div class="table_p1">
    <div class="inner">
        <div class="table_c1"></div>
    </div>
</div>
```
```css
.table_p1 {
    display: table;

    width: 200px; height: 200px;
}
.table_p1 .inner {
    display: table-cell;

    vertical-align: middle;
    text-align: center;
}
.table_p1 .table_c1 {
    display: inline-block;

    width: 100px; height: 100px;
}
```
- 原理: 通过 table-cell 的特性实现居中（table-cell的子元素应为行间元素）
    - table-cell 比较适合图片墙这类布局
- 缺点:
    - ie ...
    - 用了这就和 margin/float/absolute 说拜拜吧
    - 太麻烦

***

## inline
```html
<div class="inline_p1">
  <div class="inline_c1"> </div>
</div>
```
```css
.inline_p1 {
    width: 200px; height: 200px;

    text-align: center;
    line-height: 200px; /* 行间元素的垂直居中 */
}
.inline_p1 .inline_c1 {
    display: inline;
    font-size: 0; /* 通过内容撑开宽高 */
    padding: 50px;
}
```
- 原理: 行间元素的居中方法
- 缺点: 不灵活

***

## 伪元素
```html
<div class="before_p1">
  <div class="before_c1"> </div>
</div>
```
```css
.before_p1 {
    width: 200px; height: 200px;

    font-size: 0; /* 必须要设置 */
}
.before_p1::before {
    display: inline-block;

    content: '';
    height: 100%;
    vertical-align: middle;
}
.before_p1 .before_c1 {
    display: inline-block;

    width: 100px; height: 100px;

    vertical-align: middle;
}
```
- 原理: 通过伪元素(高度100%)为参照使 .before_c1 垂直居中
- 要点: .before_p1 的 font-size，需设为 0，否则内部会由于伪元素的 content 大小而产生偏移
    - 所以 .before_c1 中有文字时，需另设 font-size
- 缺点: 太麻烦，为了居中而居中

***

## box flexbox
### 1. box 1
```html
<div class="box_p1">
  <div class="box_c1"> </div>
</div>
```
```css
.box_p1 {
    display: -webkit-box;
    -webkit-box-pack: center;
    -webkit-box-align: center;

    width: 200px;
    height: 200px;
}
.box_p1 .box_c1 {
    width: 100px;
    height: 100px;
}
```
- 缺点: 改用 flex 吧

### box 2
```html
<div class="box_p2">
  <div class="box_c2"> </div>
</div>
```
```css
.box_p2 {
    display: flex;
    justify-content: center;
    align-items: center;

    width: 200px;
    height: 200px;
}
.box_p2 .box_c2 {
    width: 100px;
    height: 100px;
}
```
- 缺点: 除了兼容性还有其他缺点么？
