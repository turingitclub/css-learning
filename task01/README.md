任务一：零基础HTML编码
===
**面向人群：** 零基础或初学者

**难度：** 简单

重要说明
---
我们尽力保证课程内容的质量以及学习难度的合理性，但即使如此，真正决定课程效果的，还是你的每一次思考和实践。

课程多数题目的解决方案都不是唯一的，这和我们在实际工作中的情况也是一致的。因此，我们的要求不仅仅是实现设计稿的效果，更是要多去思考不同的解决方案，评估不同方案的优劣，然后使用在该场景下最优雅的方式去实现。那些最终没有被我们采纳的方案，同样也可以帮助我们学到很多知识。所以，我们列出的参考资料未必是实现需求所必须的。有的时候，实现题目的要求很简单，甚至参考资料里就有，但是背后的思考和亲手去实践却是任务最关键的一部分。在学习这些资料时，要多思考，多提问，多质疑。相信通过和小伙伴们的交流，能让你的学习事半功倍。

任务目的
---
* 了解HTML的定义、概念、发展简史
* 掌握常用HTML标签的含义、用法
* 能够基于设计稿来合理规划HTML文档结构
* 理解语义化，合理地使用HTML标签来构建页面

任务描述
---
* 参考示例图

![](./task_1_1_1.jpg)


任务注意事项
---
* 只需要完成HTML代码编写，不需要写CSS
* 示例图仅为参考，不需要完全实现一致，其中的图片、文案均可自行设定
* 尽可能多地尝试更多的HTML标签

任务协作建议
---
* 团队集中讨论，明确题目要求，保证队伍各自对题目要求认知一致
* 各自完成任务实践
* 交叉互相Review其他人的代码，建议每个人至少看一个同组队友的代码
* 相互讨论，最后合成一份组内最佳代码进行提交

在线学习参考资料
---
* [HTML 课程大纲](https://www.yuque.com/docs/share/3b4dee50-118e-422e-906f-9619b81bc1ed?#)

* [前端基础课 - 02 - HTML(上)](https://www.yuque.com/docs/share/42854e85-9ece-471d-bfd6-dfd79ee1efee?#)

* [前端基础课 - 02 - HTML(下)](https://www.yuque.com/docs/share/c29653b5-328d-4bae-afa0-b0c32790a9bb?#)

* [前端基础课 - 03 - HTML补充大纲](https://www.yuque.com/docs/share/40b23599-0e69-44f1-b155-057fba69214a?#)

* [前端基础课 - 03 - HTML(上)](https://www.yuque.com/docs/share/d95fb419-d40f-4a59-a8a0-cdea5babf53b?#)
* [前端基础课 - 03 - HTML(下)](https://www.yuque.com/docs/share/78fdf18f-d37c-4a58-af38-5c3fc5829128?#)

* [Web相关名词通俗解释](https://www.zhihu.com/question/22689579)
* [MDN HTML入门](https://developer.mozilla.org/zh-CN/docs/Web/Guide/HTML/Introduction)


学员笔记 
---

学员可将自己的笔记和遇到的问题写在这里
如： 

### 基础
1. HTML 是一种标记语言（markup language）。它告诉浏览器如何显示内容。其是网页的基础，规定了网页的结构骨架。
2. HTML由不同元素的集合组成。元素定义了它们所包含内容的语义。
    - 以下是一个网页的基本结构，其是一个树形的层次化结构，称之为 DOM: Document Object Model--文档对象模型
    ```html
    <html>
      <head>
        <title></title>
      </head>
      <body>

      </body>
    </html>
    ```
3. 元素由一对标签组成（起始标签和结束标签，特例：自闭合标签）。
4. 起始标签中可包含元素的属性。
```html
<input required> <!-- <input required=""> <input required> -->
<p class="foo">  <!-- <p class=foo> -->
```
5. HTML不区分大小写。
6. 一个HTML文档一定需要在第一行做出文档类型声明(doctype declaration)。
```html
<!DOCTYPE html>
```
7. HTML中使用特殊符号：
    - &gt; 表示大于符号">" (>)
    - &lt; 表示小于符号"<" (<)
    - &amp; 表示和符号"and"(&)
    - &quot; 表示引用符号" (")

### 元素
[元素详解](https://developer.mozilla.org/zh-CN/docs/Web/HTML/Element)

