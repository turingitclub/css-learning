# css-learning

unix -> linux
     -> macos

## 图灵IT俱乐部
<a href="https://mp.weixin.qq.com/mp/homepage?__biz=MzkyMjE4NTA4OQ==&hid=1&sn=3bdf0c94cd53644ad27b5b1d80903a60&scene=18&devicetype=android-29&version=2800015d&lang=zh_CN&nettype=ctnet&ascene=7&session_us=gh_b80e6483a6bf&pass_ticket=PgopkreabYkxOU%2FOqxsKF%2FrLlayxaDkxkphno6QjITdzpqjiA8mlE%2BpLkMFPthYH&wx_header=1&scene=1"><img border="0" src="../css-learning/asset/img/wechat-turingclub.png"/></a>


![](./img/../asset/img/微信图片_20201219151245.jpg)     
#### 介绍

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

# gitcommit规范
## Commit message 规范
`<type>: <subject>`  
&nbsp;&nbsp;&nbsp;&nbsp;注意冒号后面有空格
## type
用于说明commit的类型  
- `feat`：新功能（feature）
- `fix`：修补bug
- `docs`：文档
- `style`：格式（不影响代码运行的变动）
- `refactor`：重构（既不是新增功能,也不是修改bug代码变动）
- `test`：增加测试
- `chore`：构建过程或辅助工具的变动 如果type为`feat`和`fix`，则该 commit 将肯定出现在 Change log 之中。
## subject
subject是 commit 目的的简短描述，不超过50个字符，且结尾不加句号（.）
