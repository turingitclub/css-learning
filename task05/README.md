# 作业

css 实现页面级别 示例
https://www.quanzhanketang.com/w3css/w3css_examples.html

# pc 端

- 相册 1 （平铺）
- 相册 2 （纵向）
- 报纸
- 杂志
- 简历
- Material Design
- Mail
- 模态框登录页 1
- 模态框登录页 2
- 登录表单 1
- 登录表单 2
- 广告页 1
- 广告页 2
- 专辑详情页 1
- 游戏详情页 1
- Pricing Table
- 首页 1
- 首页 2

# 移动端
