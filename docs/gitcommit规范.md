# gitcommit规范
## Commit message 规范
`<type>: <subject>`  
&nbsp;&nbsp;&nbsp;&nbsp;注意冒号后面有空格
## type
用于说明commit的类型  
- `feat`：新功能（feature）
- `fix`：修补bug
- `docs`：文档
- `style`：格式（不影响代码运行的变动）
- `refactor`：重构（既不是新增功能,也不是修改bug代码变动）
- `test`：增加测试
- `chore`：构建过程或辅助工具的变动 如果type为`feat`和`fix`，则该 commit 将肯定出现在 Change log 之中。
## subject
subject是 commit 目的的简短描述，不超过50个字符，且结尾不加句号（.）